<?php
		if(isset($_SESSION['login']) && $_SESSION['login'] == "admin")	
{
	
	?>
	<form method="POST" action="" class="white-pink">
    <?php

    $req = 'SELECT numMonture, nomMonture, sexe, race, photoMonture, m.codeTypeMonture, c.numCavalier, nomCavalier, prenomCavalier FROM monture m LEFT JOIN cavalier c on m.numCavalier=c.numCavalier';
    $res = $connex->prepare($req);
    $res->execute();
    $lignes = $res->fetchAll();


	    ?>
    <h1>Gestions des Chevaux et Poneys :</h1>
    <br/>
    <H2> Listes de Chevaux et Poneys du Centre Equestre</H2>
    <br/>
    <table cellspacing="8" ; border="1">
        <th colspan=8> Liste des Poneys</th>
        <tr>
            <td>Photo</td>
            <td>Nom</td>
            <td>Sexe</td>
            <td>Race</td>
            <td>Propriétaire</td>
            <td>Modifier</td>
            <td>Supprimer</td>
        </tr>

        <?php
        foreach ($lignes as $ligne) {
            if ($ligne['codeTypeMonture'] == "P") { ?>
                <tr>
                    <td>
                        <img width="150px" src="images/poney/<?php echo $ligne['photoMonture']; ?>" alt=""/>
                    </td>
                    <td><?php echo $ligne['nomMonture']; ?> </td>
                    <td><?php echo $ligne['sexe']; ?> </td>
                    <td><?php echo $ligne['race']; ?> </td>
                    <td>
                        <?php if ($ligne['numCavalier'] == null) {
                            echo " Centre Equestre";
                        } else {
                            echo $ligne['nomCavalier'] . " " . $ligne['prenomCavalier'];
                        } ?>
                    </td>
                    <td><a name="modif"
                           href='index.php?page=form_modif_monture.php&monture=<?php echo $ligne['numMonture']; ?>'>MODIFIER</a>
                    </td>
                    <td><a name="suppr"
                           href='index.php?page=gestion_monture.php&monture=<?php echo $ligne['numMonture']; ?>'>SUPPRIMER</a>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
    </table>
    </br>
    </br>

    <table cellspacing="8" ; border="1">
        <th colspan=8> Liste des Chevaux</th>
        <tr>
            <td>Photo</td>
            <td>Nom</td>
            <td>Sexe</td>
            <td>Race</td>
            <td>Propriétaire</td>
            <td>Modifier</td>
            <td>Supprimer</td>
        </tr>

        <?php
        foreach ($lignes as $ligne) {
            if ($ligne['codeTypeMonture'] == "C") { ?>
                <tr>
                    <td>
                        <img width="150px" src="images/cheval/<?php echo $ligne['photoMonture']; ?>" alt=""/>
                    </td>
                    <td><?php echo $ligne['nomMonture']; ?> </td>
                    <td><?php echo $ligne['sexe']; ?> </td>
                    <td><?php echo $ligne['race']; ?> </td>
                    <td>
                        <?php if ($ligne['numCavalier'] == null) {
                            echo " Centre Equestre";
                        } else {
                            echo $ligne['nomCavalier'] . " " . $ligne['prenomCavalier'];
                        } ?>
                    </td>
                    <td><a name="modif"
                           href='index.php?page=form_modif_monture.php&monture=<?php echo $ligne['numMonture']; ?>'>MODIFIER</a>
                    </td>
                    <td><a name="suppr"
                           href='index.php?page=gestion_monture.php&monture=<?php echo $ligne['numMonture']; ?>'>SUPPRIMER</a>
                    </td>
                </tr>


            <?php } ?>
        <?php } ?>
    </table>

    <?php
    if (isset($_GET["monture"])) {
        $req1 = "DELETE FROM monture WHERE numMonture=?";
        $res1 = $connex->prepare($req1);
        $res1->execute(array(
            $_GET['monture']
        ));
    }
	}else
	echo "<h1>Accès interdit ...</h1>";

    $res->closecursor();
    ?>
</form>
