<?php
		if(isset($_SESSION['login']) && $_SESSION['login'] == "admin")	
{
	
	?>
	<form method="POST" action="" class="white-pink">
    <h1>Ajout monture</h1>
    <fieldset>
        <?php

        $req = 'SELECT numCavalier, prenomCavalier, nomCavalier FROM CAVALIER ORDER BY nomCavalier';
        $res = $connex->prepare($req);
        $res->execute();
        $lignes = $res->fetchAll();

        ?>

        <legend>Informations monture</legend>
        <label>
            <span> Nom  monture :</span>
            <input type="text" name="txtnom" placeholder="nom" required />
        </label>
        <label>
            <span>Mâle </span>
            <input type="radio" name="radioSexe" value="M" checked />
        </label>
        <label>
            <span>Femelle </span>
            <input type="radio" name="radioSexe" value="F"/>
        </label>
        <label>
            <span> Poids :</span>
            <input type="number" placeholder="100" name="txtpoids" required /> kg
        </label>
        <label>
            <span> Taille :</span>
            <input type="number" name="txttaille" placeholder="130" required /> cm
        </label>
        <label>
            <span> Poney </span>
            <input type="checkbox" name="checkType" value="P"/>
        </label>
        <label>
            <span> Race :</span>
            <input type="text" placeholder="Race cheval" name="txtrace" required />
        </label>
        <label>
            <span> Robe(couleur) </span>
            <select name="listerobe" required>
                <option selected disabled>Choisir...</option>
                <option value="Alezan">Alezan</option>
                <option value="bai">Bai</option>
                <option value="Noir">Noir</option>
                <option value="Autre">Autre</option>
            </select>
        </label>
        <label>
            <span>Nom photo :</span>
            <input type="text" name="txtphoto" placeholder="cheval.png" required />
        </label>
    </fieldset>

    <fieldset>
        <legend>Informations propriétaire</legend>
        <label>
            <span>Centre Equestre</span>
            <input type="radio" name="proprio" value="cEquestre" checked />
        </label>
        <label>
            <span>Propriétaire</span>
            <input type="radio" name="proprio" value="Proprietaire"/>
        </label>
        <select name="listecavaliers">
            <option selected disabled>Choisir ...</option>
            <?php
            foreach ($lignes as $ligne) {
                ?>
                <option value="<?php echo $ligne['numCavalier']; ?>"><?php echo $ligne['nomCavalier'] . " " . $ligne['prenomCavalier']; ?></option>
            <?php } ?>
        </select>
        <input type="submit" class="button" name="ajouter" value="ajouter"/>
    </fieldset>


    <?php
    if (isset($_POST["ajouter"])) {
        if (!isset($_POST['checkType'])) {
            $type = 'C';
        } else {
            $type = 'P';
        }

        if ($_POST['proprio'] == 'cEquestre') {
            $cavalier = null;
        } else {
            $cavalier = $_POST['listecavaliers'];
        }

        $req1 = "INSERT INTO monture VALUES (NULL,? ,? ,? ,? ,? ,? ,? ,? ,?)";
        $res1 = $connex->prepare($req1);
        $res1->execute(array(
            $_POST['txtnom'],
            $_POST['radioSexe'],
            $_POST['txtpoids'],
            $_POST['txttaille'],
            $_POST['txtrace'],
            $_POST['listerobe'],
            $_POST['txtphoto'],
            $type,
            $cavalier
        ));

        //header("Location: index.php?page=liste_monture.php");
    }
		}else
	echo "<h1>Accès interdit ...</h1>";

    $res->closecursor();
    ?>
</form>
