﻿<?php


$req = "SELECT  jour, heure, libTypeReprise, nomMoniteur, prenomMoniteur FROM planning p INNER JOIN  type_reprise tr on p.codeTypeReprise = tr.codeTypeReprise INNER JOIN  moniteur m on  tr.numMoniteur= m.numMoniteur WHERE codeTypeMonture = ? ORDER BY numPlanning ";
$stmt = $connex->prepare($req);
$stmt->execute(array(
    $_GET['type']
));

$lignes = $stmt->fetchAll();

$jour = "";
$verif = false;
?>
<h1><strong> Nos Cours :</strong></h1>
<br/>
<H2> Répartitions et horaires des cours :</H2>
<br/>
<table cellspacing="8" border="1">
    <tr>
        <td>Jours :</td>
        <td>Heures :</td>
        <td>Niveaux :</td>
        <td>Moniteurs :</td>
    </tr>
    <?php foreach ($lignes as $ligne) {
        if ($ligne['jour'] != $jour) {
            $jour = $ligne['jour'];
        } else {
            $verif = true;
        }

        ?>

        <tr>
            <td><?php if ($verif == false) {
                    echo $ligne['jour'];
                } ?></td>
            <td><?php echo date('H:i', strtotime($ligne['heure'])); ?></td>
            <td><?php echo $ligne['libTypeReprise']; ?></td>
            <td><?php echo $ligne['nomMoniteur'] . " " . $ligne['prenomMoniteur']; ?></td>
        </tr>
        <?php
        $verif = false;
    } ?>

    <?php
    $stmt->closecursor();
    ?>
</table>
