<?php
		if(isset($_SESSION['login']) && $_SESSION['login'] == "admin")	
{
	
	?>
	<form method="POST" action="" class="white-pink">
    <h1>Modification de la monture</h1>
    <fieldset>
        <?php

        $req = 'SELECT numCavalier, prenomCavalier, nomCavalier FROM CAVALIER ORDER BY nomCavalier';
        $res = $connex->prepare($req);
        $res->execute();
        $lignes = $res->fetchAll();

        $req = 'SELECT * FROM MONTURE WHERE numMonture = ?';
        $res = $connex->prepare($req);
        $res->execute(array(
                $_GET['monture']
        ));
        $infoCheval = $res->fetch();

        ?>

        <legend>Informations monture</legend>
        <label>
            <span> Nom  monture :</span>
            <input type="text" name="txtnom" value="<?php echo $infoCheval['nomMonture']; ?> " placeholder="nom" required/>
        </label>
        <label>
            <span>Mâle </span>
            <input type="radio" name="radioSexe" value="M" <?php if ($infoCheval['sexe'] == "M") echo "checked='checked'"; ?>
                   checked/>
        </label>
        <label>
            <span>Femelle </span>
            <input type="radio" name="radioSexe" value="F" <?php if ($infoCheval['sexe'] == "F") echo "checked='checked'"; ?>/>
        </label>
        <label>
            <span> Poids :</span>
            <input type="number" name="txtpoids" placeholder="100" value="<?php echo $infoCheval['poids']; ?>" required /> kg
        </label>
        <label>
            <span> Taille :</span>
            <input type="number" name="txttaille" placeholder="120" value="<?php echo $infoCheval['taille']; ?>" required /> cm
        </label>
        <label>
            <span> Poney </span>
            <input type="checkbox" name="checkType" value="P"
                   <?php if ($infoCheval['codeTypeMonture'] == "P") echo "checked='checked'"; ?>/>
        </label>
        <label>
            <span> Race :</span>
            <input type="text" name="txtrace" placeholder="Fjord" value="<?php echo $infoCheval['race']; ?>" required/>
        </label>
        <label>
            <span> Robe(couleur) </span>
            <select name="listerobe" required>
                <option value="Alezan"  <?php if ($infoCheval['robe'] == "Alezan") echo "selected"; ?> >Alezan</option>
                <option value="bai" <?php if ($infoCheval['robe'] == "bai") echo "selected"; ?>>Bai</option>
                <option value="Noir" <?php if ($infoCheval['robe'] == "Noir") echo "selected"; ?>>Noir</option>
                <option value="Autre" <?php if ($infoCheval['robe'] == "Autre") echo "selected"; ?>>Autre</option>
            </select>
        </label>
        <label>
            <span>Nom photo :</span>
            <input type="text" name="txtphoto" value="<?php echo $infoCheval['photoMonture']; ?>" required/>
        </label>
    </fieldset>

    <fieldset>
        <legend>Informations propriétaire</legend>
        <label>
            <span>Centre Equestre</span>
            <input type="radio" name="proprio" value="cEquestre" <?php if ($infoCheval['numCavalier'] == null) echo "checked"; ?> />
        </label>
        <label>
            <span>Propriétaire</span>
            <input type="radio" name="proprio" value="Proprietaire" <?php if ($infoCheval['numCavalier'] != null) echo "checked"; ?> />
        </label>
        <select name="listecavaliers">
            <option selected disabled>Choisir ...</option>
            <?php
            foreach ($lignes as $ligne) {
                ?>
                <option value="<?php echo $ligne['numCavalier']; ?>" <?php if ($infoCheval['numCavalier'] == $ligne['numCavalier']) echo "selected"; ?>><?php echo $ligne['nomCavalier'] . " " . $ligne['prenomCavalier']; ?></option>
            <?php } ?>
        </select>
    </fieldset>
    <input type="submit" class="button" name="modifier" value="modifier"/>
	<input type="hidden" name="txtNum" value="<?php echo $ligne->numMonture; ?>"/>
    <?php
    if (isset($_POST["modifier"])) {
        if (!isset($_POST['checkType'])) {
            $type = 'C';
        } else {
            $type = 'P';
        }

        if ($_POST['proprio'] == 'cEquestre') {
            $cavalier = null;
        } else {
            $cavalier = $_POST['listecavaliers'];
        }

        $req1 = "UPDATE monture
					 SET nomMonture=?, sexe=?, poids=?,taille=?, race=?, robe=?, photoMonture=?, codeTypeMonture=?, numCavalier=?
					 WHERE numMonture=?";
        $res1 = $connex->prepare($req1);
        $res1->execute(array(
            $_POST['txtnom'],
            $_POST['radioSexe'],
            $_POST['txtpoids'],
            $_POST['txttaille'],
            $_POST['txtrace'],
            $_POST['listerobe'],
            $_POST['txtphoto'],
            $type,
            $cavalier,
            $infoCheval["numMonture"]
        ));
        //header("Location: index.php?page=liste_monture.php");
    }
		}else
	echo "<h1>Accès interdit ...</h1>";

    $res->closecursor();
    ?>
</form>
