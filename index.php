<?php
			session_start(); // Démarre le système de sessions
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="images/bouton/favicon.ico" />
	<link rel="icon" type="image/png" href="images/bouton/favicon.png" />
    <meta charset="utf-8"/>
    <meta name="author" content="Solene Rochepeau"/>
    <title> Centre Equestre </title>
    <link rel="stylesheet" type="text/css" href="css/index.css"/>
	<link rel="stylesheet" type="text/css" href="css/connexion.css"/>
    <link rel="stylesheet" type="text/css" href="css/form_monture.css"/>
		
</head>
<body>
<!-- ENTETE -->
<header id="entete">
    <?php include "include/entete.php"; ?>
</header>

<!-- CONTENEUR CENTRAL  -->
<div id="centre">

    <!-- COLONNE GAUCHE  -->
    <nav class="menu" id="menu">
        <?php include "include/menu.php"; ?>
    </nav>

    <!-- CONTENU  -->
    <div id="navigation">
        <?php
        if (!isset($_GET['page'])) {
            include "include/accueil.php";
        } else {
            include "pages/" . $_GET['page'];
        }
        ?>
    </div>
</div>
<!-- PIED DE PAGE -->
<footer id="pied">
    <?php include "include/pied.php"; ?>
</footer>

</body>
</html>
