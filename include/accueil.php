﻿<h1><b>Accueil</b></h1>
<br/>
<p>Bienvenue sur notre site <strong> Centre Equestre </strong> ! 🏇 </p>
<p> Vous pouvez suivre notre actualité,<br/> vous inscrire ou encore découvrir notre espace en voyageant sur nos
    divers pages d'informations. </p>
<p> vous pouvez nous contactez via la page <a href="pages/contact.html">"Contact"</a>, <br/> ou juste en cliquant en
    dessous ⬇ ! </p>
<p> Bon séjour à vous 😊!</p>
<br/>
<img src="images/choisir.png" alt="" height=200 width=300/>
<br/>
<br/>
<p><b> N'oubliez pas, nos Chevaux et Poneys sont exceptionnels ! </b></p>
