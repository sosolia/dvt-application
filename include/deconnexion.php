<?php
session_start();
// On efface les variables de session
unset($_SESSION['login']);
unset($_SESSION['moniteur']);
// On détruit la session
session_destroy();
// On revient à la page d'index
header("Location: ../index.php");
?>
